import time

def countdown( n ):

    while n >= 0:
        print( n, end = ' ', flush = True )
        n -= 1
        time. sleep( 1 )

def finalcountdown( arr ):

    for s in arr :
        print( s, end = ' ', flush = True )
        time. sleep( 1.5 )

finalcountdown( [ "Ten", "Nine", "Eight", "Seven", "Six", "Five", "Four", "Three", "Two", "One", "Zero" ] )
#countdown( 10 )

print( "LIFT OFF!" )