def consists( arr, n ):
    for number in arr :
        if number == n :
            return True
    return False

def find_max( arr ):
    m = arr[0]
    for number in arr:
        if number > m:
            m = number
    return m
