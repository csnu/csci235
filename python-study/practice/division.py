
while True:
    number = int( input( "Enter a number: (or 0 to quit)" ))

    if( number == 0 ):
        break
    if( number % 15 == 0 ): 
        print( f"{number} is divisible for 3 or 5" )
    elif( number % 3 == 0 ):
        print( f"{number} is divisible for 3")
    elif( number % 5 == 0 ):
        print( f"{number} is divisible for 5")
    else:
        print( f"{number} is not divisible for 3 or 5" )