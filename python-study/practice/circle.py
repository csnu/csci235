import cmath

RADIUS = 15

def circle( x ) :
    y = RADIUS ** 2 - x ** 2
    y = y ** 0.5
    print( f"( {x}, {y} )" )

for i in range( 15 ) :
    circle( i )