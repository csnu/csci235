import time

def primes( n ) :
    i = 2
    isPrime = False
    while ( i < n ) :
        isPrime = True
        for div in range( 2, i // 2 + 1) :
            if( i % div != 0 ) :
                isPrime = True
            else :
                isPrime = False
                break

        if( isPrime ) :
            print( i, end = ' ', flush = True )
            time. sleep( 0.5 )

        i += 1


primes( 100 )