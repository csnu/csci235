from numbers import *
import cmath
import random
from vector import *

class Rational( Number ):
    def __init__( self, num, denom = 1 ) :
        self. num = num
        self. denom = denom

        if denom == 0 :
            raise ArithmeticError( "denominator cannot be zero" )

        self. normalize( )

    def __repr__( self ) :

        if self. denom == 1 or self. num == 0 :
            return f"{self. num}"
        else:
            return f"{self.num}/{self.denom}"
    
    def __neg__( self ) :
        return Rational( -self. num, self. denom )
        
    def __add__( self, other ) :

        if not isinstance( other, Rational ) :
            num = self. num + other * self. denom
            denom = self. denom
        else:
            num = self. num * other. denom + self. denom * other. num
            denom = self. denom * other. denom

        
        return Rational( num, denom )
    
    def __sub__( self, other ) :
        if not isinstance( other, Rational ) :
            num = self. num - other * self. denom
            denom = self. denom
        else :

            num = self. num * other. denom - self. denom * other. num
            denom = self. denom * other. denom

        return Rational( num, denom )

    def __radd__( self, other ) :

        return self.__add__( other ) 
    
    def __rsub__( self, other ) :

        return self.__sub__( other )

    def __mul__( self, other ) :
        if not isinstance( other, Rational ) :
            return Rational( self. num * other, self. denom )

        return Rational( self. num * other. num, self. denom * other. denom )

    def __rmul__( self, other ) :
        return self.__mul__( other )

    def __truediv__( self, other ) :
        if not isinstance( other, Rational ) :
            return Rational( self. num, self. denom * other )
        
        return Rational( self. num * other. denom, self. denom * other. num )

    def __rtruediv__( self, other ) :
        return self.__truediv__( other )

    def __eq__( self, other ) :
        if not isinstance( other, Rational ) :
            return self. num == other and self. denom == 1
        else :
            return self. num == other. num and self. denom == other. denom
   
    def __ne__( self, other ) :
        return not self.__eq__( other )


    def __lt__( self, other ) :
        if not isinstance( other, Rational ) :
            return self. denom < other * self. denom
        else :
            product = self. denom * other. denom
            return ( self * product ). num < ( other * product ). num 

    def __gt__( self, other ) :
        if not isinstance( other, Rational ) :
            return self. denom > other * self. denom
        else :
            product = self. denom * other. denom
            return ( self * product ). num > ( other * product ). num

    def __le__( self, other ) :
        return self < other or self == other

    def __ge__( self, other ) :
        return self > other or self == other
    
    def normalize( self ) :

        if self. num < 0 and self. denom < 0 :
            self. num *= -1
            self. denom *= -1
        elif self. denom < 0 :
            self. denom *= -1
            self. num *= -1
        elif self. num == 0 :
            self. denom = 1
            
        gcd = self. gcd( self. num, self. denom )

        self. num = self. num // gcd
        self. denom = self. denom // gcd

    def gcd( self, n1, n2 ) :
        n1 = abs( n1 )
        n2 = abs( n2 )

        gcd = 1

        if n1 == 0 and n2 == 0 :
            raise ArithmeticError( "gcd(0,0) does not exist" )

        if n1 > n2: 
            small = n2 
        else: 
            small = n1
        for i in range(1, small+1): 
            if(( n1 % i == 0) and ( n2 % i == 0 )): 
                gcd = i 
                
        return gcd