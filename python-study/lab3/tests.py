from matrix import *
from vector import *
from rational import *

def tests( ) :
    
    m1 = Matrix( 
                Rational( 1, 2 ), 
                Rational( 1, 3 ), 
                Rational( -2, 7 ),
                Rational( 2, 8 ) 
                )

    m2 = Matrix( 
                Rational( -1, 3 ), 
                Rational( 2, 7 ), 
                Rational( 2, 5 ),
                Rational( -1, 7 ) 
                )
    
    m3 = Matrix( 
                Rational( 1, 4 ), 
                Rational( 2, 5 ), 
                Rational( 3, 7 ),
                Rational( 7, 8 ) 
                )

    I = Matrix( Rational( 1 ),           # Identity matrix
                Rational( 0 ),         
                Rational( 0 ),        
                Rational( 1 ) )

    v = Vector( 1, 0 )

    print( m1 @ m2 )

    print( m1. inverse( ))

    # Prooving Matrix multiplication is ASSOCIATIVE
    if ( m1 @ m2 ) @ m3   ==   m1 @ (m2  @ m3 ) :
        print( "Matrix multiplication is associative" )
    else :
        raise ValueError( "Prooving Matrix multiplication is associative FAILED" )



    # Matrix multiplication with addition is DISTRIBUTIVE
    if m1 @ ( m2 + m3 ) == ( m1 @ m2 ) + m1 @ m3 and (m1 + m2 ) @ m3 == ( m1 @ m3 ) + m2 @ m3 :
        print( "Matrix multiplication with addition is distributive" )
    else :
        raise ValueError( "Prooving Matrix multiplication is distributive FAILED" )



    # Matrix multiplication corresponds to composition of application
    if  m1 ( m2(v) ) == ( m1 @ m2 ) (v) :
        print( "Matrix multiplication corresponds to composition of application" )
    else :
        print( "Prooving Matrix multiplication corresponds to composition of application FAILED")



    # Determinant commutes over multiplication
    if m1. determinant( ) * m2. determinant( ) == ( m1 @ m2 ). determinant( ) :
        print( "Determinant commutes over multiplication")
    else :
        raise ValueError( "Prooving Determinant commutes over multiplication FAILED")



    # Matrix is inverse
    if m1 @ m1. inverse( ) == I  and m1. inverse( ) @ m1 == I :
        print( "Matrix IS inverse")
    else :
        raise ValueError( "Matrix is not inverse" )


tests( )