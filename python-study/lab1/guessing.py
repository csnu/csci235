import random

def guess( ) :
    upperbound = 1000
    secret = random.randrange( upperbound )
    # print( f"secret = {secret}" )
    print( "hello, please guess my secret number" )
    print( f"it lies between 0 and {upperbound-1}\n" )

    attempts = 0
    while True:

        attempts += 1
        
        numberString = input( "Enter a number: " )
        
        try :
            #(put the read command here)
            guess = int( numberString )
        except ValueError:
            print( f"{numberString} is not a number" )
            continue

        if guess > secret:
            print( "TOO HIGH!!!" )
        elif guess < secret:
            print( "TOO LOW!!!" )
        else:
            print( f"\nSecret number is {secret}. Congrats!!!")
            print( f"Attemps count: {attempts}" )
            return

guess( )