import cmath

def change( money ):

    dollar = 0
    quarter = 0
    dime = 0
    nickel = 0
    penny = 0

    if money > 1:
        dollar = int( money )
        temp = round( ( money - dollar ) * 100 )
        while temp > 0:
            if ( temp >= 25  ):
                quarter += 1
                temp -= 25
                print( f"value left {temp}" )
                continue
            elif( temp >= 10 ):
                dime += 1
                temp -= 10
                print( f"value left {temp}" )
                continue
            elif( temp >= 5):
                nickel += 1
                temp -= 5
                print( f"value left {temp}" )
                continue
            else:
                print( f"value left {temp}" )
                penny = temp
                temp = 0

    print( f"${money} is as follows:")
    print( f"1$ x {dollar}" )
    print( f"quarter x {quarter}" )
    print( f"dime x {dime}" )
    print( f"nickel x {nickel}" )
    print( f"penny x {penny}" )

while True:
    number = float( input( "Please enter the value: "))
    change( number )
    if number == 0:
        break