
import random
import time

class CantMove( Exception ) :

   def __init__( self, reason ) : 
      self. __reason = reason

   def __repr__( self ) :
      return "unable to find a move: {}". format( self.__reason )

#---------------------------------------------------------------------------------------------------------------------
class Nim :
   def __init__( self, startstate ) :
      self. state = startstate


   # Goal is to be unambiguous : 

   def __repr__( self ) :
      
      string = ""

      for i in range( len( self.state )) :
         string +=  ( str( i + 1 )  + ":\t" )             # adding 1, 2, 3, ... to string
         
         for j in range( self.state[i] ) :                # adding string representation of sticks to string 
            string += "1 "

         string += "\n"
      
      return string
   

   # Return sum of all rows:

   def sum( self ) :

      total = 0

      for i in self. state :                       # loop iteration through self. state and adding each stick to total 
         total += i
      
      return total

   # Return nimber (xor of all rows): 
   
   def nimber( self ) :

      nimber = self. state[0]
      
      for i in range( len( self. state ) - 1) :
         nimber = nimber ^ ( self. state[ i + 1 ] )               # for each iteration nimber = nimber xor ( consequtive state values )

      #nim = "{0:b}".format(nim)
      
      return nimber
      
   # Make a random move, raise a CantMove if
   # there is nothing left to remove. 

   def randommove( self ) :
      while True:
         row = random. randrange(0, len( self. state ))           # randomly selected row

         if self. state[row] > 0 :  
            self. state[row] = random. randrange( 0, self. state[row] )  # if row non-empty, assign it random number between 0 and row's value
            return

   # Try to force a win with misere strategy.
   # This functions make a move, if there is exactly
   # one row that contains more than one stick.
   # In that case, it makes a move that will leave
   # an odd number of rows containing 1 stick.
   # This will eventually force the opponent to take the
   # last stick.
   # If it cannot obtain this state, it should raise
   # CantMove( "more than one row has more than one stick" )

   def removelastmorethantwo( self ) :
      index = -1                          # index of row containing more than one stick
      count = 0                           # number of rows who has more than one stick
      nonemptyrows = 0                    # number of non-empty rows

      for i in range( len( self. state )) :
         if self. state[i] > 1:
            count += 1
            index = i
         
      for i in self. state:
         if i > 0 :
            nonemptyrows += 1
      
      if count == 1 :                     
         if( nonemptyrows % 2 != 0 ) :       
            self. state[index] = 1        # assign row we has more than one stick with 1 if we have odd number of rows
         else :
            self. state[ index ] = 0      # assign row we has more than one stick with 0 if we have even number of rows
      else :
         raise CantMove( "more than one row has more than one stick" )

   # Try to find a move that makes the nimber zero.
   # Raise CantMove( "nimber is already zero" ), if the
   # nimber is zero already.

   def makenimberzero( self ) :
      
      nimber = self. nimber( )
      
      if nimber == 0 :
         raise CantMove( "nimber is already zero" )
      else :
         while True:
            row = random. randrange( 0, len( self. state ))       # randomly chosen row
            if self. state[row] > 0 :
               if self. state[row] ^ nimber < self. state[row]: 
                  self. state[row] = self. state[row] ^ nimber
                  return
 
   def optimalmove( self ) :

      index = 0
      count = 0
      
      for i in range( len( self. state )) :
         if self. state[i] > 1:
            count += 1
            index = i

      if count == 1 :
         self. removelastmorethantwo( )
      else :
         try :
            self. makenimberzero( )
         except CantMove:
            self. randommove( )

   # Let the user make a move. Make sure that the move
   # is correct. This function never crashes, not
   # even with the dumbest user in the world. 
           
   def usermove( self ) :
      
      while True:
         try: 
            row = int( input( "Select a row to delete: " ))

            if row < 0 or row > len( self. state ) : 
               print( "\nThat row does not exist. Try again...\n" )
               continue
            
            if self. state[row - 1] == 0 :
               print( "\nThat row is empty. Try again...\n" )
               continue

            sticks = int( input( "How many sticks to remain? "))

            if sticks < 0 or sticks > self. state[row - 1] :
               print( "\nInvalid bounders for sticks to remove. Try again...\n" )
               continue
         
            self. state[row - 1] = sticks
            return
         except ValueError: 
            print( "\nEnter numbers!!!\n" )
            continue

#---------------------------------------------------------------------------------------------------------



def play( ) :
    st = Nim( [ 1, 2, 3, 4, 5, 6 ] )
    turn = 'user'
    while st. sum( ) > 1 :
        if turn == 'user' :
            print( "\n" )
            print( st )
            print( "hello, user, please make a move" )
            st. usermove( )
            turn = 'computer'
        else :
            print( "\n" )
            print( st )
            print( "now i will make a move\n" )
            print( "thinking" )
            for r in range( 15 ) :
                print( ".", end = "", flush = True )
                time. sleep( 0.1 )
            print( "\n" )
            st. optimalmove( )
            turn = 'user'
    print( "\n" )

    if turn == 'user' :
        print( "you lost\n" )
    else :
        print( "you won\n" )

play( )