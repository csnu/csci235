//***************************************************************************************************************************************************
//
// Copyright (C) 2019 Selim Temizer.
//
// This file is part of Printer Example.
//
// Printer Example is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// Printer Example is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with Printer Example. If not, see <http://www.gnu.org/licenses/>.
//
//***************************************************************************************************************************************************




//***************************************************************************************************************************************************


//***************************************************************************************************************************************************




//***************************************************************************************************************************************************

public class PrintStringsThread implements Runnable
{
  //=================================================================================================================================================

    Thread          thread ;
    String          str1   ;
    String          str2   ;
    PrintTwoStrings pts    ;
  //Object          o      ;
  //Semaphore       s      ;

  //=================================================================================================================================================

  //public PrintStringsThread ( String str1 , String str2 , PrintTwoStrings pts , Object o )
  //public PrintStringsThread ( String str1 , String str2 ,                       Object o )
  //public PrintStringsThread ( String str1 , String str2 , Semaphore       s              )
    public PrintStringsThread ( String str1 , String str2 , PrintTwoStrings pts            )
    {
        this.str1 = str1 ;
        this.str2 = str2 ;
        this.pts  = pts  ;
      //this.o    = o    ;
      //this.s    = s    ;

      thread = new Thread( this ) ;

      thread.start() ;
    }

  //=================================================================================================================================================

  @Override public void run ()
  {
      try                                { Thread.sleep( (int) ( Math.random() * 100 ) ) ; }
      catch ( InterruptedException ex )  { /* Do nothing */                                }

    // s.acquire() ;
    // PrintTwoStrings.print( str1 , str2 ) ;
    // s.release() ;

    //synchronized ( o   )
      synchronized ( pts )
      {
          pts.print( str1 , str2 ) ;
        //PrintTwoStrings.print( str1 , str2 ) ;
      }
  }

  //=================================================================================================================================================
}

//***************************************************************************************************************************************************
