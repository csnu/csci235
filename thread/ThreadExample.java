//***************************************************************************************************************************************************
//
// Copyright (C) 2019 Selim Temizer.
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
//
//***************************************************************************************************************************************************




//***************************************************************************************************************************************************

class SimpleThread extends Thread
{
  //=================================================================================================================================================

  public SimpleThread ( String name )
  {
    super( name ) ;

    // start() ;
  }

  //=================================================================================================================================================

  @Override public void run ()
  {
    for ( int i = 0 ; i < 100 ; i++ )
    {
      System.out.println( getName() ) ;

      try                     { Thread.sleep( 20 ) ; }
      catch ( Exception ex )  { /* Do nothing */     }
    }
  }

  //=================================================================================================================================================
}

//***************************************************************************************************************************************************




//***************************************************************************************************************************************************

public class ThreadExample
{
  //=================================================================================================================================================

  public static void main ( String args[] ) throws Exception
  {
    new SimpleThread( "Jamaica" ).start() ;  Thread.sleep( 300 ) ;
    new SimpleThread( "Fiji"    ).start() ;

    ThreadGroup group = Thread.currentThread().getThreadGroup() ;

    Thread[] tarray = new Thread[ 10 ] ;

    int actualSize = group.enumerate( tarray ) ;

    for ( int i = 0 ; i < actualSize ; i++ )
    {
      System.out.println( "Thread " + tarray[ i ].getName() + " in thread group " + group.getName() ) ;
    }
  }

  //=================================================================================================================================================
}

//***************************************************************************************************************************************************
