public class PrintNameRunnable implements Runnable {

    Thread thread;

    public PrintNameRunnable( String name ) {
        thread = new Thread( this, name );
        thread. start( );       
    }
    
    public void run( ) {
        for( int i = 0; i < 100; i ++ ) 
            System.out.print( thread. getName( ));
    }
}
