//***************************************************************************************************************************************************
//
// Copyright (C) 2019 Selim Temizer.
//
// This file is part of Printer Example.
//
// Printer Example is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// Printer Example is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with Printer Example. If not, see <http://www.gnu.org/licenses/>.
//
//***************************************************************************************************************************************************




//***************************************************************************************************************************************************

public class PrintTwoStrings
{
  //=================================================================================================================================================

  // static
  // synchronized
  public void print ( String str1 , String str2 )
  {
    System.out.print( str1 ) ;

    try                                { Thread.sleep( 500 ) ; }
    catch ( InterruptedException ie )  { /* Do nothing */      }

    System.out.println( str2 ) ;
  }
  

  //=================================================================================================================================================
}

//***************************************************************************************************************************************************
