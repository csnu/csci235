//***************************************************************************************************************************************************
//
// Copyright (C) 2019 Selim Temizer.
//
// This file is part of Printer Example.
//
// Printer Example is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// Printer Example is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License along with Printer Example. If not, see <http://www.gnu.org/licenses/>.
//
//***************************************************************************************************************************************************




//***************************************************************************************************************************************************

//***************************************************************************************************************************************************




//***************************************************************************************************************************************************

public class PrintStringsTest
{
  //=================================================================================================================================================

  public static void main ( String args[] ) throws Exception
  {
      PrintTwoStrings pts1 = new PrintTwoStrings() ;
    //PrintTwoStrings pts2 = new PrintTwoStrings() ;
    //PrintTwoStrings pts3 = new PrintTwoStrings() ;

    //String    s   = "Selim"            ;
    //Semaphore sem = new Semaphore( 1 ) ;

    new PrintStringsThread( "Hello "     , "there."     , pts1 ) ;
    new PrintStringsThread( "How are "   , "you?"       , pts1 ) ;
    new PrintStringsThread( "Thank you " , "very much!" , pts1 ) ;
  }

  //=================================================================================================================================================
}

//***************************************************************************************************************************************************
