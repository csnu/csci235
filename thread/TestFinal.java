public class TestFinal {
    public static void main(String[] args) {
        MyThread thread = new MyThread();
        thread.hello("hello world").start();
    }
}

class MyThread extends Thread {

    private String s = "" + "String".length( );
    
    public void run( ) {
        System.out.println("Run" + s. length());
        s = "1" + s.length();
    }

    public Thread hello( String s ) {
        run( );
        System.out.println( s. toUpperCase());
        return this;
    }
}