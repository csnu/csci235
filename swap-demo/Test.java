import java.util.Scanner;

class Int {
	
	public  int data;

	public Int() {}

	public Int(int data) {
		this.data = data;
	}
}

class Swap {
	public static void swap(Int i1, Int i2) {
		int temp = i1.data;
		i1.data = i2.data;
		i2.data = temp;
	}

	public static void swap(Integer i1, Integer i2) {
		int temp = i1;
		i1 = i2;
		i2 = temp;
	}
}

public class Test {
	public static void main(String[] args) {
/*
		Int i1 = new Int(3);
		Int i2 = new Int(4);
*/
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter two integer: ");
	
		Integer i1 = sc.nextInt();
		Integer i2 = sc.nextInt();

		System.out.println("Before swap: i1 = " + i1 + " i2 = " + i2);
		Swap.swap(i1, i2);
		System.out.println("After swap: i1 = " + i1 + " i2 = " + i2);

		sc.close();
	}
}