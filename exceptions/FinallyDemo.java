public class FinallyDemo {
    public static void main(String[] args) throws Exception {
        
        int n = 2;
        try {
            switch(n) {
                case 1: System.out.println("1st case;"); break;
                case 3: System.out.println("3rd case"); throw new RuntimeException("3!");
                case 4: System.out.println("4th case"); throw new RuntimeException("4!");
                case 2: System.out.println("2nd case"); //throw new RuntimeException("3!");
            }
        } catch(RuntimeException e) {
            System.out.println("Runtime Exception: " + e. getMessage());
        } finally {
            System.out.println("finally");
        }
    
    }
}