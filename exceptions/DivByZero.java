public class DivByZero {
    public static void main(String[] args) {
        
        
        try {
            System.out.println( "try block" );
            System.out.println(1/0);
            System.out.println("Hello, pls print me.");
            System.out.println( "try block 2" );
        } catch(ArithmeticException e) {
            System.out.println(e);
            System.out.println( "catch block" );
        } catch( NullPointerException e ) {
            System.out.println( "null pointer catch block" );
        } finally {
            System.out.println( "finally block");
        }
    }
}