import java.util.*;

public class StateMap {

    public Map< String, String > stateMap;

    public StateMap( ) {
        stateMap = new HashMap<String, String>();
        for( String state[]: states ) {
            stateMap. put( state[0], state[1] );
        }
    }

    public Map<String, String> getStateMap() {
        return this. stateMap;
    }

    public String[][] getStateArray( ) {
        return this. states;
    }

    private String[][] states = {
        { "Alabama", "AL"}, 
        { "Alaska", "AK" },
        { "Arizona", "AZ"}
    };
}