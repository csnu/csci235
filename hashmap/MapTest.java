import java.util.*;

public class MapTest {
    public static void main(String[] args) {
        StateMap states = new StateMap();
        Map<String, String> stateAbbr = states. getStateMap();

        System.out.println( stateAbbr. get( "Alabama" ));
        System.out.println( stateAbbr. get( "Arizona" ));
        System.out.println( stateAbbr. get( "Alaska" ));
    }
}