import java.util.Date;

public class Staff extends Employee {
    
    private String title;

    public Staff( ) { }

    public Staff( String name, String address, String number, String email, 
                    String office, double salary, Date dateHired, String title ) {
        super( name, address, number, email, office, salary, dateHired );
        this. title = title;
    }

    @Override
    public String toString( ) {
        return super. toString( ) + "\nTitle: " + this. title;
    }
}