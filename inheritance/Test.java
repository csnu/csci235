import java.util.Date;

public class Test {
    public static void main(String[] args) {
        Person p = new Person( "Makhambet", "Kabanbay batyr, 51", "8 (707) 669 81 51", "makhambet.torezhan@nu.edu.kz");      
        
        Student student = new Student( "Makhambet", "Kabanbay batyr, 51", "8 (707) 669 81 51", "makhambet.torezhan@nu.edu.kz", "Sophomore" );

        Person employee = new Employee( "Makhambet", "Kabanbay batyr, 51", "8 (707) 669 81 51", "makhambet.torezhan@nu.edu.kz", 
                                        "A113", 1500, new Date( ));

        Employee faculty = new Faculty( "Makhambet", "Kabanbay batyr, 51", "8 (707) 669 81 51", "makhambet.torezhan@nu.edu.kz", 
                                        "A113", 1500, new Date( ), "8:00-17:00", "Assistant to the Regional Manager");

        Employee staff = new Staff( "Makhambet", "Kabanbay batyr, 51", "8 (707) 669 81 51", 
        "makhambet.torezhan@nu.edu.kz", "A113", 1500, new Date( System.currentTimeMillis( ) - 1000000), "Front-End Developers" );

        System.out.println("name: " + p. getName( ));
        System.out.println( "status" + student. getStatus( ));
        System.out.println( employee. toString( ));
        System.out.println( faculty. toString( ));
        System.out.println( staff. toString( ));
    }
}