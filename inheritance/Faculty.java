import java.util.Date;

public class Faculty extends Employee {
    
    private String officeHours;
    private String rank;

    public Faculty( ) { }

    public Faculty( String name, String address, String number, String email, 
                    String office, double salary, Date dateHired, String officeHours, String rank ) {
        super( name, address, number, email, office, salary, dateHired );
        this. officeHours = officeHours;
        this. rank = rank;
    }

    @Override
    public String toString( ) {
        return super. toString( ) + "\nOffice Hours: " + this. officeHours + "\nRank: " + this. rank;
    }
}