public class Person {
    
    private String name;
    private String address;
    private String number;
    private String email;

    public Person( ) { }

    public Person( String name, String address, String number, String email ) {
        this. name = name;
        this. address = address;
        this. number = number;
        this. email = email;
    }

    public String getName( ) { return this. name; }
    public String getAddress( ) { return this. address; }
    public String getNumber( ) { return this. number; } 
    public String getEmail( ) { return this. email; }

    public void setName( String name ) { this. name = name; }
    public void setAddress( String address ) { this. address = address; }
    public void setNumber( String number ) { this. number = number; }
    public void setEmail( String email ) { this. email = email; }

    public String toString( ) {
        return "Person: \nName: " + this. name + 
                       "\nAddress: " + this. address + 
                       "\nPhone Number: " + this. number + 
                       "\nEmail: " + this. email;
    }
}