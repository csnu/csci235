import java.util.Date;

public class Employee extends Person {
    
    private String office;
    private double salary;
    private Date dateHired;

    public Employee( ) {
        this. dateHired = new Date( );
    }
    
    public Employee(  String name, String address, String number, String email, String office, double salary, Date dateHired ) { 
        super( name, address, number, email );
        this. office = office;
        this. salary = salary;
        this. dateHired = dateHired;
    }

    public String getOffice( ) { return this. office; }
    public double getSalary( ) { return this. salary; }
    public Date getdateHired( ) { return this. dateHired; }

    public void setOffice( String office ) { this. office = office; }
    public void setSalary( double salary ) { this. salary = salary; }
    public void setDateHired( Date dateHired ) { this. dateHired = dateHired; }

    @Override
    public String toString( ) {
        return super. toString( ) + "\nEmployee: " + "\nOffice: " + this. office + 
                                    "\nSalary: $" + this. salary + "\nDate hired: " + this.dateHired; 
    }
}