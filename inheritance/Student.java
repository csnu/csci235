public class Student extends Person {
    
    private String status;

    public Student( ) {  }

    public Student( String name, String address, String number, String email, String status ) {
        super( name, address, number, email );
        this. status = status;
    }

    public String getStatus( ) { return this. status; }
    public void setStatus( String status ) { this. status = status; }
     
    @Override
    public String toString( ) {
        return super. toString( ) + "\nStudent: " +  "\nStatus: " + this. status;
    }
}