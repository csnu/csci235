import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class TestArray {

	public static void print( List< Integer > arr ) {

		System.out.print( "[ " );

		for( int i = 0; i < arr. size( ); i ++ ) {
			if( i != 0 )
				System.out.print( ", " );
			System.out.print( arr. get(i) );

		}
		System.out.println( " ]" );
	}

	
	public static void main( String[] args ) {
		
		ArrayList<Integer> arr = new ArrayList<>();
		List<Integer> list = new LinkedList<>();

		for( int i = 1 ; i < 10; i ++ ) {
			arr. add( i );
		}
		
		for( int i = 0; i < 10; i ++ ) 
			list. add( i + 5);

		print( arr );
		print( list );
	}
}
