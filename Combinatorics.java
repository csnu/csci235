import java.util.ArrayList;

public class Combinatorics {

    public static boolean equals( int a, int b ) {
        return a == b;
    }

    public static boolean isSumNine( int a, int b, int c, int d ) {
        return a + b + c + d == 9;
    }

    public static boolean areThreeEqualDigits( int a, int b, int c, int d ) {
        return ( a == b && b == c ) || ( b == c && c == d ) || ( a == c && c == d ) || ( a == b && b == d );
    }

    public static boolean areDistinct( int a, int b, int c, int d ) { 

        ArrayList<Integer> lst = new ArrayList<>();

        if( !lst.contains(a)) lst.add(a);

        if( !lst.contains(b)) lst.add(b);

        if( !lst.contains(c)) lst.add(c);

        if( !lst.contains(d)) lst.add(d);

        return lst. size( ) == 4;        
    }
    public static void main(String[] args) {
        
        final int FROM = 1000;
        final int TO = 9999;

        int a = FROM;
        int b = TO;

        int x1, x2, x3, x4;

        
        int x = TO;
        int count = 0;
        for( int i = a; i < b; i ++ ) { 
            
            try {
                //Thread.sleep(10);
                x = i;
                x1 = x % 10;
                x /= 10;
                x2 = x %10;
                x /= 10;
                x3 = x % 10;
                x /= 10;
                x4 = x % 10;
                if( isSumNine( x1, x2, x3, x4 )) {
                    System.out.println(x4 + " " + x3 + " " + x2 + " " + x1);
                    count++;
                }
            } catch( Exception e) {} 
            
        }

        System.out.println("\nCount = " + count);
    }
}