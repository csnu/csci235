import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import javax.imageio.ImageIO;

public class UniversityMap extends Entity {
    
    private String mapLink = "https://sun9-20.userapi.com/c857636/v857636752/9021d/dqrnIHdTi68.jpg";
    
    @Override
    public void draw( Graphics2D g2d ) {
        
        try {
            BufferedImage image = ImageIO.read( new URL( mapLink ));
            g2d.drawImage(image, 0, 0, Common.windowWidth, Common.windowHeight, null);
        } catch( IOException e) {
            System.out.println("No file detected");
        }
    }

    @Override
    public void step() {
    
    }    
    
}
