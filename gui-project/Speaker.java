import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

public class Speaker extends Entity {
    
    private String speakerName;
    
    public Speaker( String speakerName ) {
        this. speakerName = speakerName;
    }

    @Override
    public void draw( Graphics2D g2d ) {
        
        g2d. setColor( Color.black );
        g2d. setFont( new Font( "Arial", Font.BOLD, 14 ));

        try {
            BufferedImage image = ImageIO.read( new File("Demo/" + speakerName + ".gif"));

            if( speakerName. equals("Nazarbayev")) {
                g2d. drawImage( image, 948, 446, 50, 70, null );
                g2d. drawString( speakerName, 948, 446);
            } else {
                g2d. drawImage( image, 729, 446, 50, 70, null );
                g2d. drawString( speakerName, 729, 446);
            }
        } catch( IOException e) {
            System.out.println("No file detected");
        }
        
    }

    @Override
    public void step() {
    }
}
