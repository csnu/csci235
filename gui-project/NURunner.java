import javax.swing.JFrame;
import javax.swing.Timer;

public class NURunner {
    
    public JFrame window;
    public Display display;
    public Common common;    
    
    public NURunner() {
        window = new JFrame();
        display = new Display();
        common = new Common();

        window.add(display);
               
        window.pack();
       
        window.setSize(display.getPrefferedSize());
        window.setTitle("University Map Game");
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setVisible(true);
        
        Timer timer = new Timer( 2000, (e) -> {
            common .stepAllEntities() ;
            display.repaint        () ;
        });
        
        timer. start( );
    }
    public static void main(String[] args) {
        NURunner nuRunner = new NURunner();
    }
}
