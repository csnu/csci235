public abstract class State {
    
    public boolean isOver;
    
    public boolean isVisible;
    
    public abstract void step(Entity e);
}
