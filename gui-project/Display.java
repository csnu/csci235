import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JPanel;
import javax.swing.Timer;

public class Display extends JPanel {
    public Common common = new Common();
    
    public Display() { }

    public Dimension getPrefferedSize() {
        return new Dimension( Common. windowWidth, Common. windowHeight );
    }
    
    @Override
    public void paintComponent( Graphics g ) {
        super. paintComponent(g);
        
        Graphics2D g2d = ( Graphics2D ) g;
        
        common. drawAllEntities( g2d );
    }
    
    
}
