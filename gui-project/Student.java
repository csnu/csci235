import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;

public class Student extends Entity {
    
    public int grade;
    public String studentName;    
    Vector2D vect;// super.position;
    private int x, y;
    public Student( String studentName ) { 
        
        this. studentName = studentName;
        vect = new Vector2D();
        x = Common.randomInt( 50, Common.windowWidth - 50);
        y = Common.randomInt( 50, Common.windowHeight - 50);
    }
    
    @Override
    public void draw( Graphics2D g2d ) {
        
        if( grade < 100) {
            g2d. setColor( Color.CYAN );
            g2d. fillOval( this.x, this.y, 30, 30 );
            g2d. setColor( Color.black );
            g2d. setFont( new Font( "Arial", Font.BOLD, 14 ));
            g2d. drawString( this.studentName, this.x, this.y - 10);
            g2d. drawString( this.grade + "", this.x + 7, this.y + 17 );
        } else {
            g2d. setColor( Color. MAGENTA );
            x = 830 + this. studentName. length( );
            y = 474 + this. studentName. length( );
            g2d. fillOval( x, y, 30, 30 );
            
            g2d. setColor( Color.black );
            g2d. setFont( new Font( "Arial", Font.BOLD, 14 ));
            g2d. drawString( this.studentName, this.x, this.y - 10);
            g2d. drawString( this.grade + "", this.x + 7, this.y + 17 );
        }
    }

    @Override
    public void step( ) {
        this.x = this.x + 10;
        //vect.set(vect.plus(new Vector2D(10, 0)));
        //vect.y++;
    }
}
