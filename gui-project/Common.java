import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Common {
    
    public final static int windowWidth = 1200;
    public final static int windowHeight = 600;
    public UniversityMap map = new UniversityMap();
    public List<Academician> academicians = new ArrayList<>();
    public List<Speaker> speakers = new ArrayList<>();
    public List<Student> students = new ArrayList<>();
    public List<Assessment> assessments;
    
    private final List<String> studentNames = new ArrayList<>( Arrays. asList(
                            "Dante", 
                            "Giovanni",
                            "Homer",
                            "Ovid",
                            "Abay")
                        );
    private final List<String> profNames = new ArrayList<>( Arrays. asList(
                            "Tourassis", 
                            "Katsu",
                            "Temizer",
                            "Nivelle")
                        );
    
    public Common() { 
        
        for( int i = 0; i < studentNames. size() ; i++ ) {
            students. add( new Student( studentNames.get(i)));
        }
        
        for( int i = 0; i < profNames. size( ); i ++ ) 
            academicians. add( new Academician( profNames.get(i)));
        
        speakers. add( new Speaker( "Nazarbayev" ));
        speakers. add( new Speaker( "Tokayev" ));
    }
    
    public static int randomInt(int from, int to) {
        
        Random random = new Random();
        if( from == 0 )
           return random.nextInt(to);
        else
            return (int) (Math.random() * (to - from) + from);
    }
    
    public void stepAllEntities() {
        for( Student student: students ) {
            student.step();
        }
    }
    
    public void drawAllEntities(Graphics2D g2d) {
        int total = 0;
        
        map.draw(g2d);
                
        for( int i = 0; i < students. size() ; i++ ) {
            students.get(i). draw( g2d );
            total += students.get(i).grade;
        } 
       
        for( Academician academician: academicians ) {
            academician. draw( g2d);
        }
        
        if ( total >= 500 ) {
            for( Speaker speaker: speakers )
                speaker. draw( g2d );
        }
    }
}
