public class Vector2D {
    
    public int x;
    public int y;
    
    public Vector2D( ) { }
    
    public Vector2D( int x, int y ) {
        this. x = x;
        this. y = y;
    }
    
    public void set( Vector2D v ) {
        this. x = v. x;
        this. y = v. y;
    }
    
    public double distanceTo( Vector2D other ) {
        return Math.sqrt((other.x - this.x) * (other.x - this.x) + 
                (other.y - this.y) * (other.y - this.y) );
    }
    
    public Vector2D normalize( ) {
        return new Vector2D();
    }
    
    public Vector2D plus(Vector2D other) {
        Vector2D vect = new Vector2D();
        vect.x = this.x + other.x;
        vect.y = this.y + other.y;
        
        return vect;
    }
    
    public Vector2D minus(Vector2D other) {
        Vector2D vect = new Vector2D();
        vect.x = this.x - other.x;
        vect.y = this.y - other.y;
        
        return vect;
    }
}
