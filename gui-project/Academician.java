import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

public class Academician extends Entity {
    
    public String acadName;
    
    public Academician( String acadName ) { 
        this. acadName = acadName;
    }
    
    @Override
    public void draw( Graphics2D g2d) {
        
        int x = Common.randomInt( 100, Common.windowWidth - 100 );
        int y = Common.randomInt( 100, Common.windowHeight - 100 );
       
        try {
            BufferedImage image = ImageIO.read( new File("Demo/" + acadName + ".gif"));
            g2d.drawImage(image, x, y, 50, 70, null);
        } catch( IOException e) {
            System.out.println("No file detected");
        }
        g2d. setColor( Color.black );
        g2d. setFont( new Font( "Arial", Font.BOLD, 14 ));
        g2d. drawString( acadName, x, y);
    }

    @Override
    public void step( ) {
        //position.set(position.plus(new Vector2D(10, 0)));
    }
}
