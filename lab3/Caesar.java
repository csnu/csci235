public class Caesar implements Cipher {
    
    @Override
    public String encrypt( String alphabet, String plainText, String key ) {
        
        String encrypted = "";
        plainText = plainText. toUpperCase( );

        for( int i = 0; i < plainText. length( ); i ++ ) {
            encrypted += getChar(alphabet, plainText. charAt(i), key, true);
        }

        return encrypted;
    }

    @Override
    public String decrypt( String alphabet, String cipherText, String key ) {

        String decrypted = "";
        cipherText = cipherText. toUpperCase( );

        for( int i = 0; i < cipherText.length( ); i ++ ) {
            decrypted += getChar(alphabet, cipherText. charAt(i), key, false);
        }

        return decrypted;
    }

    public static char getChar( String alphabet, char ch, String key, boolean encrypt ) {

        final int SIZE = alphabet. length( );
        
        int index = -1;
        int steps = Integer. parseInt( key ) % alphabet. length( );

        for( int i = 0; i < SIZE; i ++ ) {
            if( ch == alphabet. charAt(i)) {
                
                if ( encrypt )
                    index = ( i + steps + SIZE ) % 26;
                else
                    index = ( i - steps + SIZE ) % 26;
                                
                break;
            }
        }
        return alphabet.charAt( index );
    }
}