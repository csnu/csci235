public class Vigenere implements Cipher {

    public String encrypt( String alphabet, String plainText, String key ) {
     
        String encrypted = "";
        
        for( int i = 0, j = 0; i < plainText. length( ); i ++, j++ ) {
            if( j == key.length( )) 
                j = 0;
            encrypted += getChar( alphabet, plainText. charAt(i), key. charAt(j), true);
        }

        return encrypted;
    }

    public String decrypt( String alphabet, String cipherText, String key ) {
        
        String decrypted = "";
        
        for( int i = 0, j = 0; i < cipherText. length( ); i ++, j ++ ) {
            if( j == key.length( )) 
                j = 0;
            decrypted += getChar( alphabet, cipherText. charAt(i), key. charAt(j), false);
        }

        return decrypted;
    }

    public static char getChar( String alphabet, char ch, char key, boolean encrypt ) {

        final int SIZE = alphabet. length( );

        int index = -1;
        for( int i = 0; i < SIZE; i ++ ) {
            if( ch == alphabet. charAt(i)) {

                if( encrypt ) 
                    index = ( i + alphabet. indexOf(key) + SIZE) % 26; 
                else
                    index = ( i - alphabet. indexOf(key) + SIZE ) % 26; 
                              
                break;
            }
        }
        return alphabet.charAt( index );
    }
}