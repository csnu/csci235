import java.util.Scanner;

public class CSCI {
    
    public static void displayMenu( ) {
        System.out.println("CSCI Cipher Operations: ");
        System.out.println("-----------------------------------");
        System.out.println("1. Decrypt and verify Caesar cipher");
        System.out.println("2. Decrypt and verify Viginere cipher");
        System.out.println("3. Display this menu again");
        System.out.println("4. Quit");
    }

    public static void main(String[] args) {
        
        Scanner input = new Scanner( System. in );
        
        int choice;
        String alphabet, plainText, cipherText, key, reencrypted;

        Cipher caesar = new Caesar( );
        Cipher viginere = new Vigenere( );        

        displayMenu( );

        do {
            System.out.print("\nChoice : ");
            
            choice = input. nextInt( );
            
            System.out.println();

            if( choice < 1 || choice > 4 ) {
            
                System.out.println( "Invalid choice! Try again." );
                continue;
            
            } else {
            
                switch( choice ) {
                    case 1:
                        System.out.print("Enter alphabet    : ");
                        alphabet = input. next( ); 
                        
                        System.out.print("Enter cipher text : ");
                        cipherText = input. next( );
                        
                        System.out.print("Enter key         : ");
                        key = input. next( );

                        System.out.println();

                        plainText = caesar. decrypt( alphabet, cipherText, key );
                        reencrypted = caesar. encrypt( alphabet, plainText, key);

                        System.out.println("Plain text                              : " + plainText);
                        System.out.println("Plain text re-encrypted for verification: " + reencrypted);
                        System.out.println("Are cipher text and encrypted text equal: " + cipherText. equals( reencrypted ));
                        break;

                    case 2:
                        System.out.print("Enter alphabet    : ");
                        alphabet = input. next( ); 
                        
                        System.out.print("Enter cipher text : ");
                        cipherText = input. next( );
                        
                        System.out.print("Enter key         : ");
                        key = input. next( );

                        System.out.println();

                        plainText = viginere. decrypt( alphabet, cipherText, key );
                        reencrypted = viginere. encrypt( alphabet, plainText, key);

                        System.out.println("Plain text                              : " + plainText);
                        System.out.println("Plain text re-encrypted for verification: " + reencrypted);
                        System.out.println("Are cipher text and encrypted text equal: " + cipherText. equals( reencrypted ));
                        break;
                    case 3:
                        displayMenu( ); break;
                }
            }
        } while( choice != 4 );
    
        System.out.println( "Have a nice crime-free day!" );         
    }
}