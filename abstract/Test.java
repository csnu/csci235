import java.util.ArrayList;

public class Test {
    public static void main(String[] args) {
        ArrayList<Shape> shapes = new ArrayList<>();
        shapes.add( new Circle(5));
        shapes.add( new Rectangle(3, 4));

        double sum = 0;
        for( Shape shape: shapes ) {
            System.out.println("area: " + shape. getArea( ));
            sum += shape. getArea( );
        }

        System.out.println( "sum: " + sum );
    }
}