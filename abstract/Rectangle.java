public class Rectangle extends Shape {

    private double width, height;

    public Rectangle( ) { }

    public Rectangle( double width, double height ) {
        this. width = width;
        this. height = height;
    }

    @Override
    public double getArea( ) {
        return this. width * this. height;
    }

    @Override
    public double getPerimeter( ) {
        return 2 * ( this. width + this. height );
    }
}