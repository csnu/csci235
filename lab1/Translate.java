public class Translate {
	
	public static int indexOf(String word, String[] array ){
		for(int i = 0; i < array.length; i++ ) {
			if(word.equals(array[i])) {
				return i;
			}
		}

		return -1;
	} 
	public static boolean contains(String word, String[] array ) {
		for( int i = 0; i < array.length; i++) {
			if(word.equals(array[i]))
				return true;
		}

		return false;
	}	
	public static void main(String[] args) {

		if (args.length !=  2) {
			System.out.println("Wrong input"); 
		}
		String[] eng = { "Hi", "Book", "Pen", "School", "Teacher" };
		String[] tur = { "Merhaba", "Kitap", "Kalem", "Okul", "Ogretmen" };
		String[] kaz = { "Salem", "Kitap", "Kalam", "School", "Mugalim" };

		String word, language;

		word = args[0];
		language = args[1];

		int index;

		if( contains(word, eng) && language != "EN" ) {
			index = indexOf(word, eng);

			if( language.equals( "TR" ) ) {
				System.out.println(tur[index] + " ( TR )");
			} else if (language.equals("KZ")) {
				System.out.println(kaz[index] + " ( KZ )");
			} else {
				System.out.println("Wrong language!");
			}

		} else if (contains(word, kaz) && language != "KZ") {
			index = indexOf(word, kaz);

			if( language.equals( "TR" ) ) {
				System.out.println(tur[index] + " ( TR )");
			} else if (language.equals("EN")) {
				System.out.println(eng[index] + " ( EN )");
			} else {
				System.out.println("Wrong language!");
			}

		} else if (contains(word, tur) && language != "TR") {
			index = indexOf(word, tur);

			if( language.equals( "EN" ) ) {
				System.out.println(eng[index] + " ( EN )");
			} else if (language.equals("EN")) {
				System.out.println(kaz[index] + " ( KZ )");
			} else {
				System.out.println("Wrong language!");
			}
		}
	}
}