public class Integrate
{
	public static double f ( double x )
 	{
 		return 3 * Math.sin(2 * x) + 1.5 ;
 	}
 	public static double integrate ( double from , double to , double dx )
 	{
 		double total = 0;
 		for( double d = from; d <= to; d += dx ) {
 			total += f(d) * dx;
 		}

 		return total;
 	}
 	public static void main ( String [] args )
 	{
 	// Check number of arguments
	 	if (args.length != 3) {
 			System.out.println("Wrong input");
 		}
 	// Parse arguments

 	// Calculate and print result

 		Double from = Double.parseDouble(args[0]);
 		Double to = Double.parseDouble(args[1]);
 		Double dx = Double.parseDouble(args[2]);
 		System.out.println("Integration (approx) = " + integrate(from, to, dx));
 	}
}